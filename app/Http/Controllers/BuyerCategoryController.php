<?php

namespace App\Http\Controllers;

use App\Models\Buyer;

class BuyerCategoryController extends ApiController
{
    public function index(Buyer $buyer)
    {
        $categories = $buyer->transactions()
            ->with('product.categories')
            ->get()
            ->pluck('product.categories')
            ->collapse()
            ->unique('id')
//            ->flatten()
            ->values();

        return $this->showAll($categories);
    }
}
