<?php


namespace App\Http\Controllers\Product;


use App\Http\Controllers\ApiController;
use App\Models\Category;
use App\Models\Product;
use App\Transformers\CategoryTransformer;
use Illuminate\Http\Request;

class ProductCategoryController extends ApiController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only('index');
        $this->middleware('auth:api')->except('index');
        $this->middleware('scope:manage-product')->except('index');

        $this->middleware('can:add-category, product')->only('update');
        $this->middleware('can:delete-category, product')->only('destroy');

    }

    private function getCategories(Product $product)
    {
        return $product->categories;
    }
    public function index(Product $product)
    {
        return $this->getCategories($product);
    }

    public function update(Request $request, Product $product, Category $category)
    {
        $product->categories()->syncWithoutDetaching($category->id);
        return $this->getCategories($product);
    }

    public function destroy(Product $product, Category $category)
    {
        if(! $product->categories->find($category->id)) {
            return $this->errorResponse("The product does not belong to the specified category!", 404);
        }
        $product->categories()->detach($category->id);
        return $this->getCategories($product);
    }
}
