<?php


namespace App\Scopes;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use PhpParser\Builder;

class BuyerScope implements Scope
{

    public function apply(\Illuminate\Database\Eloquent\Builder $builder, Model $model)
    {
        $builder->has('transactions');
    }
}
